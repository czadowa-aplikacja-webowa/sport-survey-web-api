from flask import Flask, render_template, redirect, request
from flask_sqlalchemy import SQLAlchemy
import pandas as pd
from scipy.stats import chi2_contingency
from scipy.stats import spearmanr
from scipy.stats import pearsonr
from datetime import datetime
import statistics

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///form.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 'True'

db = SQLAlchemy(app)


class FormData(db.Model):
    __tablename__ = 'formdata'
    id = db.Column(db.Integer, primary_key=True)
    year = db.Column(db.Integer)
    type_of_study = db.Column(db.String)
    university = db.Column(db.String)
    age = db.Column(db.Integer)
    sex = db.Column(db.String)
    weight = db.Column(db.Integer)
    height = db.Column(db.Integer)
    diet_choice = db.Column(db.String)
    amount_of_beer = db.Column(db.Integer)
    amount_of_wine = db.Column(db.Integer)
    amount_of_vodka = db.Column(db.Integer)
    sleep_time = db.Column(db.String)
    training_time = db.Column(db.String)
    sportstyle = db.Column(db.String)
    is_training_alone = db.Column(db.Boolean)
    is_training_in_sport_center = db.Column(db.Boolean)
    is_multisport = db.Column(db.Boolean)
    sportsman_behaviour = db.Column(db.String)
    date_created = db.Column

    def __init__(self, year, type_of_study, university, age, sex,
                 weight, height, diet_choice, amount_of_beer,
                 amount_of_wine, amount_of_vodka, sleep_time,
                 training_time, sportstyle, is_training_alone,
                 is_training_in_sport_center, is_multisport,
                 sportsman_behaviour):
        self.year = year
        self.type_of_study =type_of_study
        self.university = university
        self.age = age
        self.sex = sex
        self.weight = weight
        self.height=height
        self.diet_choice=diet_choice
        self.amount_of_beer = amount_of_beer
        self.amount_of_wine = amount_of_wine
        self.amount_of_vodka =amount_of_vodka
        self.sleep_time = sleep_time
        self.training_time = training_time
        self.sportstyle = sportstyle
        self.is_training_alone = is_training_alone
        self.is_training_in_sport_center = is_training_in_sport_center
        self.is_multisport = is_multisport
        self.sportsman_behaviour =sportsman_behaviour

db.create_all()


@app.route("/")
def welcome():
    return render_template('welcome.html')

@app.route("/form")
def show_form():
    return render_template('form.html')

@app.route("/project")
def project_page():
    return render_template('project.html')

@app.route("/about_us")
def aboutus_page():
    return render_template('about_us.html')

@app.route("/raw")
def show_raw():
    formData = db.session.query(FormData).all()
    return render_template('raw.html', formdata=formData)


THESIS1 = ""
THESIS2 = ""
THESIS3 = ""
THESIS4 = ""
THESIS5 = ""
THESIS6 = ""

def data_analysis():
    form_data_list = db.session.query(FormData).all()
    year = []
    type_of_study = []
    university = []
    age = []
    sex = []
    weight = []
    height = []
    diet_choice = []
    amount_of_beer = []
    amount_of_wine = []
    amount_of_vodka = []
    sleep_time = []
    training_time = []
    sportstyle = []
    is_training_alone = []
    is_training_in_sport_center = []
    is_multisport = []
    sportsman_behaviour = []

    for i, row in enumerate(form_data_list, 1):

        year.append(row.year)
        type_of_study.append(row.type_of_study)
        university.append(row.university)
        age.append(row.age)
        sex.append(row.sex)
        weight.append(row.weight)
        height.append(row.height)
        diet_choice.append(row.diet_choice)
        amount_of_beer.append(row.amount_of_beer)
        amount_of_wine.append(row.amount_of_wine)
        amount_of_vodka.append(row.amount_of_vodka)
        sleep_time.append(row.sleep_time)
        training_time.append(row.training_time)
        sportstyle.append(row.sportstyle)
        is_training_alone.append(row.is_training_alone)
        is_training_in_sport_center.append(row.is_training_in_sport_center)
        is_multisport.append(row.is_multisport)
        sportsman_behaviour.append(row.sportsman_behaviour)
    data = list(zip(year, type_of_study, university, age, sex, weight, height, diet_choice, amount_of_beer,
                    amount_of_wine, amount_of_vodka, sleep_time, training_time, sportstyle, is_training_alone,
                    is_training_in_sport_center, is_multisport, sportsman_behaviour))
    df = pd.DataFrame(data, columns=['year', 'type_of_study', 'university', 'age', 'sex', 'weight', 'height',
                                     'diet_choice', 'amount_of_beer', 'amount_of_wine', 'amount_of_vodka', 'sleep_time',
                                     'training_time', 'sportstyle', 'is_training_alone', 'is_training_in_sport_center',
                                     'is_multisport', 'sportsman_behaviour'])
    df['alcohol'] = df['amount_of_beer'] + 1.2 * df['amount_of_wine'] + 1.5 * df['amount_of_vodka']
    df['bmi'] = df['weight']/df['height']/df['height']


    #Theory 1 - AGH students are more physically active than UJ students
    print('Teza 1 - AGH bardziej aktywny sportowo od UJ')
    agh_mask = df['university']== 'agh'
    uj_mask = df['university']== 'uj'
    times, uniques = pd.factorize(df['training_time'], sort=True) #Factorize changes categories to numbers. Here unique values are properly ordered
                                                       #['less than 1 hour', '1-2 h', '3-4 h', '5-6 h', '6 h <']

    stat_mono_university_training_time, p_mono_university_training_time = spearmanr(df['university'][agh_mask | uj_mask], times[agh_mask | uj_mask])
    if stat_mono_university_training_time > 0:
        THESIS1 = f'Teza obalona z prawdopodobieństwem {100-p_mono_university_training_time*100:.2f}%.'
    elif stat_mono_university_training_time < 0:
        THESIS1 = f'Teza potwierdzona z prawdopodobieństwem {100-p_mono_university_training_time*100:.2f}%.'
    #print(df[['university', 'training_time']]) - for manual validation


    # Theory 2 - You work out more if you own Multisport card
    print('Teza 2 - Osoby posiadające kartę multisport ćwiczą zauważalnie więcej')
    stat_mono_is_multisport_training_time, p_mono_is_multisport_training_time = spearmanr(df['is_multisport'], times)
    if stat_mono_is_multisport_training_time > 0:
        THESIS2 = f'Teza potwierdzona z prawdopodobieństwem {100 - p_mono_is_multisport_training_time * 100:.2f}%.'
    elif stat_mono_is_multisport_training_time < 0:
        THESIS2 = f'Teza obalona z prawdopodobieństwem {100 - p_mono_is_multisport_training_time * 100:.2f}%.'
    #print(df[['is_multisport', 'training_time']])


    # Theory 3 - On later years people have more spare time and you it for physical prowess
    print('Teza 3 - Na póżniejszych latach studenci mają więcej wolnego czasu i poświęcają go aktywności fizycznej')
    stat_mono_year_training_time, p_mono_year_training_time = spearmanr(df['year'], times)
    if stat_mono_year_training_time > 0:
        THESIS3 = f'Teza potwierdzona z prawdopodobieństwem {100 - p_mono_year_training_time * 100:.2f}%.'
    elif stat_mono_year_training_time < 0:
        THESIS3 = f'Teza obalona z prawdopodobieństwem {100 - p_mono_year_training_time * 100:.2f}%.'


    # Theory 4 - Field of study is correlated with Time spent on sport and thus some fields have more spare time than others
    print('Teza 4 - Studenci niektórych kierunków mają więcej wolnego czasu i poświęcają go aktywności fizycznej')
    encoded_type, code_type = pd.factorize(df['type_of_study'])

    flag = False
    for each in code_type:
        stat_mono_type_of_study_training_time, p_mono_type_of_study_training_time = spearmanr(df['type_of_study']== each, times)
        if p_mono_type_of_study_training_time < 0.2:
            if stat_mono_type_of_study_training_time > 0:
                THESIS4 = f'Kierunki o charakterze {each}m Poświęcają zauważalnie więcej czasu na sport niż inne. Prawdopobienstwo {100 - p_mono_type_of_study_training_time * 100:.2f}%'
                flag = True
            elif stat_mono_type_of_study_training_time < 0:
                THESIS4 = f'Kierunki o charakterze {each}m Poświęcają zauważalnie mniej czasu na sport niż inne. Prawdopobienstwo {100 - p_mono_type_of_study_training_time * 100:.2f}%'
                flag = True
    if flag == False:
        print('Teza została obalona')
    else:
        print('Teza została potwierdzona')


    #Theory 5
    print('Teza 5 - Studenci niektórych uczelni spożywają więcej alkoholu niż inne')
    encoded_univeristy, code_university = pd.factorize(df['university'])

    flag = False
    for each in code_university:
        stat_mono_university_alcohol, p_mono_university_alcohol = spearmanr(
            df['university'] == each, df['alcohol'])
        if p_mono_university_alcohol < 0.2:
            if stat_mono_university_alcohol > 0:
                THESIS5 = f'Studenci uczelni {each.upper()} poświęcają zauważalnie więcej czasu na alkohol niż inne. Prawdopobienstwo {100 - p_mono_university_alcohol * 100:.2f}%'
                flag = True
            elif stat_mono_university_alcohol < 0:
                THESIS5 = f'Studenci uczelni {each.upper()} poświęcają zauważalnie mniej czasu na alkohol niż inne. Prawdopobienstwo {100 - p_mono_university_alcohol * 100:.2f}%'
                flag = True
    if flag == False:
        print('Teza została obalona')
    else:
        print('Teza została potwierdzona')

    # Theory 6 People that consume more alcohol work out less
    print('Teza 6 - Spozywanie alkoholu jest skorelowane z czasem poświęconym ćwiczeniom')
    stat_mono_alcohol_training_time, p_mono_alcohol_training_time = spearmanr(df['alcohol'], times)
    if stat_mono_alcohol_training_time > 0:
        THESIS6 = f'Ludzie spożywający więcej alkoholu ćwiczą więcej z prawdopodobieństwem {100-p_mono_university_training_time*100:.2f}%.'
    elif stat_mono_alcohol_training_time < 0:
        THESIS6 = f'Ludzie spożywający więcej alkoholu ćwiczą mniej z prawdopodobieństwem{100-p_mono_university_training_time*100:.2f}%.'

    # Theory 7 - People that eat healthy are more likely to do one sport than other.
    print("Teza 7 - Osoby prowadzące zdrowy styl życia uprawiają jedne sporty częsciej od innych")
    food_encoded, food_code = pd.factorize(df['diet_choice'], sort=True)
    # print(food_encoded, food_code)
    types_of_sport = ['Bieganie', 'plywanie', 'fitness', 'jazda na rowerze', 'windsurfing', 'jazda na nartach',
                      'snowboard', 'jazda na lyzwach', 'jazda na rolkach', 'jazda konna', 'tenis', 'tenis stolowy',
                      'wspinaczka', 'pilka nozna', 'taniec', 'koszykowka', 'siatkowka', 'pilka reczna', 'silownia',
                      'joga']
    # print([df['sportstyle'].str.contains('pilka nozna')])
    flag = False
    for each in types_of_sport:
        mask = df['sportstyle'].str.contains(each)
        if mask.any():
            stat_mono_sportstyle_diet_choice, p_mono_sportstyle_diet_choice = spearmanr(mask, food_encoded)
            if p_mono_sportstyle_diet_choice < 0.2:
                if stat_mono_sportstyle_diet_choice > 0:
                    print(
                        f'Osoby kierujące się zasadami zdrowego odżywiania częściej uprawiają {each}. Prawdopobienstwo {100 - p_mono_sportstyle_diet_choice * 100:.2f}%')
                    flag = True
                elif stat_mono_sportstyle_diet_choice < 0:
                    print(
                        f'Osoby kierujące się zasadami zdrowego odżywiania częściej uprawiają {each} Prawdopobienstwo {100 - p_mono_sportstyle_diet_choice * 100:.2f}%')
                    flag = True
    if flag == False:
        print('Teza została obalona')
    else:
        print('Teza została potwierdzona')


@app.route("/tmp")
def show_tmp():
    return render_template('tmp.html')

@app.route("/results")
def show_results():
    form_data_list = db.session.query(FormData).all()
    data_analysis()

    women = 0
    men = 0
    for el in form_data_list:
        if el.sex == "woman":
            women += 1
        elif el.sex == "mezczyzna":
            men += 1
    # Prepare data for google charts
    sex = [['Kobiety', women], ['Meżczyźni', men]]

    first = 0
    second = 0
    third = 0
    fourth = 0
    fifth = 0
    for el in form_data_list:
        if el.year == 1:
            first += 1
        elif el.sex == 2:
            second += 1
        if el.year == 3:
            third += 1
        elif el.sex == 4:
            fourth += 1
        if el.year == 5:
            fifth += 1
    year = [['I', first], ['II', second], ['III', third], ['IV', fourth], ['V', fifth]]

    techniczny = 0
    human = 0
    przyr = 0
    art = 0
    ekonom = 0
    med = 0
    for el in form_data_list:
        if el.type_of_study == "Humanistyczno-społeczny":
            techniczny += 1
        elif el.type_of_study == "Przyrodniczy/ścisły":
            human += 1
        if el.type_of_study == "Techniczny":
            przyr += 1
        elif el.type_of_study == "Ekonomiczno-biznesowy":
            art += 1
        if el.type_of_study == "Artystyczny":
            ekonom += 1
        if el.type_of_study == "Medyczny":
            med += 1
    type_of_study = [['Techniczny', techniczny], ['Humanistyczno-społeczny', human], ['Przyrodniczy/ścisły', przyr],
                     ['Artystyczny', art], ['Ekonomiczno-biznesowy', ekonom], ['Medyczny', med]]
    is_multisport = []
    agh = 0
    pk = 0
    uj = 0
    uek = 0
    up = 0
    ur = 0
    inn = 0
    alcohol_agh = 0
    alcohol_pk = 0
    alcohol_uj = 0
    alcohol_uek = 0
    alcohol_up = 0
    alcohol_ur = 0
    alcohol_inn = 0
    for el in form_data_list:
        if el.university == "agh":
            agh += 1
            alcohol_agh = alcohol_agh + el.amount_of_beer + el.amount_of_wine + el.amount_of_vodka
        elif el.university == "pk":
            pk += 1
            alcohol_pk = alcohol_pk + el.amount_of_beer + el.amount_of_wine + el.amount_of_vodka
        if el.university == "uj":
            uj += 1
            alcohol_uj = alcohol_uj + el.amount_of_beer + el.amount_of_wine + el.amount_of_vodka
        elif el.university == "uek":
            uek += 1
            alcohol_uek = alcohol_uek + el.amount_of_beer + el.amount_of_wine + el.amount_of_vodka
        if el.university == "up":
            up += 1
            alcohol_up = alcohol_up + el.amount_of_beer + el.amount_of_wine + el.amount_of_vodka
        elif el.university == "ur":
            ur += 1
            alcohol_ur = alcohol_ur + el.amount_of_beer + el.amount_of_wine + el.amount_of_vodka
        if el.university == "other":
            inn += 1
            alcohol_inn = alcohol_inn + el.amount_of_beer + el.amount_of_wine + el.amount_of_vodka

    alcohol_univ = [['alcohol_agh', alcohol_agh], ['alcohol_pk', alcohol_pk], ['alcohol_uj', alcohol_uj],
                  ['alcohol_uek', alcohol_uek], ['alcohol_up', alcohol_up],
                  ['alcohol_ur', alcohol_ur], ['alcohol_inn', alcohol_inn]]

    sleep01 = 0
    sleep12 = 0
    sleep34 = 0
    sleep56 = 0
    sleep6 = 0

    for el in form_data_list:
        if el.sleep_time == "0-1 hour":
            sleep01 += el.amount_of_beer + 1.2 * el.amount_of_wine + 1.5 * el.amount_of_vodka
        elif el.sleep_time == "1-2 h":
            sleep12 += el.amount_of_beer + 1.2 * el.amount_of_wine + 1.5 * el.amount_of_vodka
        if el.sleep_time == "3-4 h":
            sleep34 += el.amount_of_beer + 1.2 * el.amount_of_wine + 1.5 * el.amount_of_vodka
        elif el.sleep_time == "5-6 h":
            sleep56 += el.amount_of_beer + 1.2 * el.amount_of_wine + 1.5 * el.amount_of_vodka
        if el.sleep_time == "6 h <":
            sleep6 += el.amount_of_beer + 1.2 * el.amount_of_wine + 1.5 * el.amount_of_vodka

    sleep_drink = [['0-1 hour', sleep01], ['1-2 h', sleep12], ['3-4 h', sleep34],
              ['5-6 h', sleep56], ['6 h <', sleep6]]

    university = [['Akademia Górniczo-Hutnicza', agh], ['Politechnika Krakowska', pk], ['Uniwersytet Jagielloński', uj],
              ['Uniwersytet Ekonomiczny w Krakowie', uek], ['Uniwersytet Pedagogiczny w Krakowie', up],
              ['Uniwersytet Rolniczy w Krakowie', ur], ['Inna', inn]]
    sportsman_behaviour = []

    # return render_template('results.html', THESIS1=THESIS1, THESIS2=THESIS2, THESIS3=THESIS3,
    #                        THESIS4=THESIS4, THESIS5=THESIS5, THESIS6=THESIS6, universities=university_dict)
    return render_template('results.html', sex=sex, year=year,
                           type_of_study=type_of_study, university=university,
                           sleep_drink=sleep_drink, alcohol_univ=alcohol_univ)


@app.route("/save", methods=['POST'])
def save_form_answer():
    year = request.form['year']
    type_of_study = request.form['type_of_study']
    university = request.form['university']
    age = request.form['age']
    sex = request.form['sex']
    weight = request.form['weight']
    height = request.form['height']
    diet_choice = request.form['diet_choice']
    amount_of_beer = request.form['amount_of_beer']
    if amount_of_beer is None:
        amount_of_beer = 0
    amount_of_wine = request.form['amount_of_wine']
    if amount_of_wine is None:
        amount_of_wine = 0
    amount_of_vodka = request.form['amount_of_vodka']
    if amount_of_vodka is None:
        amount_of_vodka = 0
    sleep_time = request.form['sleep_time']
    training_time = request.form['training_time']
    sportstyle = ';'.join(request.form.getlist('sportstyle'))
    is_training_alone = request.form['training_with'] == "sam"
    is_training_in_sport_center = request.form['training_in_sport_center'] == "W osrodku"
    is_multisport = request.form['multisport'] == "Ma multisport"
    sportsman_behaviour = request.form['sportsman_behaviour']

    formData = FormData(year, type_of_study, university, age, sex,
                 weight, height, diet_choice, amount_of_beer,
                 amount_of_wine, amount_of_vodka, sleep_time,
                 training_time, sportstyle, is_training_alone,
                 is_training_in_sport_center, is_multisport,
                 sportsman_behaviour)
    db.session.add(formData)
    db.session.commit()
    #data_analysis()
    return redirect('/')





if __name__ == "__main__":
    app.debug = True
    app.run()
